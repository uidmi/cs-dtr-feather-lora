# CommenSense Daughter Board #


### Daughter board v2.0 test points ###

| Labels        | Signal           | Comments  |
| ------- |:-------------| -----|
| T1      | MEAS_CODEC |  |
| T2     | EN_CODEC      |    |
| T3 | EN_MIC      |     |
| T4      | MEAS_MIC |  |
| T5     | MEAS_BARO      |    |
| T6 | MEAS_IMU      |     |
| T7      | EN_BARO |  |
| T8     | EN_IMU      |    |
| T9 | MEAS_RFM      |     |
| T10      | MEAS_SAMD |  |
| T11     | EN_RADIO      |    |
| EN | EN_MEAS      |     |
| AINT      |  | ATTINY interrupt |
| ASCL     |       |  ATTINY I2C clock  |
| ASDA |       |   ATTINY I2C data  |
| CS1      | BUS0_SPI_CS_SAMD21|  |
| CS2     | BUS0_SPI_CS_CODEC      |    |
| MISO | BUS0_SPI_MISO      |     |
| MOSI      | BUS0_SPI_MOSI |  |
| SCK     | BUS0_SPI_SCK      |    |
| SCL | BUS1_I2C_SCL      |     |
| SDA      | BUS1_I2C_SDA |  |
| D_IN     | DTR_DETECT_IN      |    |
| D_OUT | DTR_DETECT_IN      |     |
| USBV |       |  VBUS from micro-USB   |
| RESET |       |  Connect to ground to reset the audio codec   |


**Note:** MEAS_xxx enables power measurements; EN_xxx signal turns the compoenet on/off