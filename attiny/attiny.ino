
#include <Wire.h>
 
#define SLAVE_ADDR 9

#define MAX_DTR_NUMBER 5
#define MAX_DEV_NUMBER 4

#define DEV_TYPE_MASK    0x84
#define DEV_VERSION_MASK  0x88


typedef enum {
  R_LoRa = 0,   
  R_WiFi,   
  R_BLE,    
  
} S_TYPE;


struct dev_info {
  S_TYPE      dev_type;
  uint8_t     vers;
};

struct dtr_info {
  uint8_t       addr;
  int8_t        number_devs; 
  struct dev_info   devs[MAX_DEV_NUMBER];
};


byte cmd; 
struct dtr_info mydtr = {
    SLAVE_ADDR,
    1,
    {R_LoRa, 0x12}
  };

void setup() {
  Wire1.begin(SLAVE_ADDR); // Initialize I2C communications as Slave

  Wire1.onRequest(requestEvent);
  Wire1.onReceive(receiveEvent);
  
  SerialUSB.begin(9600); // Setup Serial Monitor
  while(!SerialUSB); //Wait for the serial to set up
  SerialUSB.println("Attiny");
}
 
void receiveEvent(int b) {
  while (0 < Wire1.available()) {
    cmd = Wire1.read();
  }
  
  // Print to Serial Monitor
  SerialUSB.print("Slave - receive cmd");
  SerialUSB.print(cmd);
  SerialUSB.println(" from the master.");
}
 
void requestEvent() {
  byte response = 0, idx;

  if(cmd == 0){
    response = mydtr.number_devs;
  }else if (cmd & 0x80){
    idx = cmd & 0x03;
    if(cmd & 0x04){
      response = mydtr.devs[idx].dev_type;
    }
    if(cmd & 0x08){
      response = mydtr.devs[idx].vers;
    }
  }
  
  
  // Send response back to Master
  Wire1.write(response);
  SerialUSB.print("Slave - send ");
  SerialUSB.print(response);
  SerialUSB.println(" to the master.");}
 
void loop() {
   delay(1000);
}
