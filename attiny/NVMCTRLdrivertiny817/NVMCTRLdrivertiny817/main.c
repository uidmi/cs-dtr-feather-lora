#include <atmel_start.h>
#include <stdio.h>
#define MAX_COMMAND_LEN 16

uint8_t                   wdata[] = {0, 1, 2, 3};
uint8_t                   rdata[6];
uint8_t                   expected[6] = {0xff, 0, 1, 2, 3, 0xff};
volatile nvmctrl_status_t status;
volatile uint8_t          rb;
uint8_t                   i;

volatile uint8_t I2C_0_slave_address;
volatile uint8_t I2C_0_register_address;
volatile uint8_t I2C_0_num_addresses;
volatile uint8_t I2C_0_num_reads;
volatile uint8_t I2C_0_num_writes;

char command[MAX_COMMAND_LEN];

void verify_error()
{
	printf("Verify Error\r\n");
	while (1)
		;
};

void cmd_error()
{
	printf("Command Error\r\n");
	while (1)
		;
};

void readLine(){
	char c ;
	uint8_t index = 0;
	while(1){
		c = USART_0_read();
		if(c != '\n' && c != '\r')
		{
			command[index++] = c;
			if(index > MAX_COMMAND_LEN)
			{
				index = 0;
				printf("Input exceeds maximum length\r\n");
				return;
			}
		}
		
		if(c == '\n' ){
			command[index] = '\0';
			index = 0;
			return;
		}
	}

	
}
/*
NOTE:
Before executing this program, make sure the BOOTEND fuse is set so that the
BOOT section is large enough to hold this program, but small enough to leave
room for the APPLICATION section which is programmed by this code.
The reason for this is stated in the tiny817 datasheet under
"Inter-Section Write Protection":
 * Code in the BOOT section can write to APPCODE and APPDATA.

This code was tested with a BOOTEND setting of 0x8.

Note that the INTCTRL in the START project has been configured to move
INTVEC to start of BOOT section. This is done as the USART-driven
printf uses interrupts, and INTVEC are by default located at
start of APPCODE. Setting BOOTEND to something different from 0
causes start of APPCODE to be different from 0.
If we didn't relocate the INTVEC by writing IVSEL, we would have
to use the linker to place the ISR at start of APPCODE.

*/

void I2C_address_handler()
{
	I2C_0_slave_address = I2C_0_read();
	printf("I2C address received: ");
	printf(I2C_0_slave_address);
	printf("\r\n");
	I2C_0_send_ack(); // or send_nack() if we don't want to ack the address
	I2C_0_num_addresses++;
}

void I2C_read_handler()
{ // Master read operation
	I2C_0_write(0x0c);
	printf("I2C read received\r\n ");
	I2C_0_num_reads++;
}

void I2C_write_handler()
{ // Master write handler
	I2C_0_register_address = I2C_0_read();
	printf("I2C write received: ");
	printf(I2C_0_register_address);
	printf("\r\n");
	I2C_0_send_ack(); // or send_nack() if we don't want to receive more data
	I2C_0_num_writes++;
}

void test_write_flash_byte(uint16_t inside_of_app_section)
{
	printf("-->Test WF1\r\n");
	// Test write of one byte to flash: Write 0xca
	status = FLASH_0_write_flash_byte(inside_of_app_section, NULL, 0xca);
	if (status != NVM_OK) {
		cmd_error();
	}
	// Read back and check result
	rb = FLASH_0_read_flash_byte(inside_of_app_section);
	if (rb != 0xca) {
		verify_error();
	}
	printf("--> Passed\r\n");
}

void test_write_flash_block(uint16_t inside_of_app_section)
{
	// Test write of many bytes to flash. Show that the write can cross page boundaries.
	printf("Test WF2\r\n");

	// Erase both flash pages containing the region to write with 0 to prove addresses outside block to write are
	// unchanged.
	status = FLASH_0_erase_flash_page(inside_of_app_section - PROGMEM_PAGE_SIZE);
	if (status != NVM_OK) {
		cmd_error();
	}

	status = FLASH_0_erase_flash_page(inside_of_app_section);
	if (status != NVM_OK) {
		cmd_error();
	}

	// Write 4 bytes crossing the boundary of these pages
	status = FLASH_0_write_flash_block(inside_of_app_section - 2, wdata, 4, NULL);
	if (status != NVM_OK) {
		cmd_error();
	}

	// Read back and check result
	for (i = 0; i < 6; i++)
		rdata[i] = FLASH_0_read_flash_byte(inside_of_app_section - 3 + i);

	for (i = 0; i < 6; i++)
		if (rdata[i] != expected[i]) {
			verify_error();
		}

	printf("--> Passed\r\n");
}

void test_write_eeprom_byte()
{
	uint16_t adr = 35; // Write to eeprom address 35
	// Test write of one byte to eeprom: Write 0xba
	printf("Test WE1\r\n");
	status = FLASH_0_write_eeprom_byte(adr, 0xca);
	if (status != NVM_OK) {
		cmd_error();
	}

	// Read back and check result
	rb = FLASH_0_read_eeprom_byte(adr);
	if (rb != 0xca) {
		verify_error();
	}
	printf("--> Passed\r\n");
}

void test_write_eeprom_block()
{
	uint16_t adr = 89;

	// Test write of many bytes to eeprom.
	printf("Test WE2\r\n");

	// Write 4 bytes to this eeprom address
	status = FLASH_0_write_eeprom_block(adr, wdata, 4);
	if (status != NVM_OK) {
		cmd_error();
	}

	// Read back and check result
	FLASH_0_read_eeprom_block(adr, rdata, 4);

	for (i = 0; i < 4; i++)
		if (rdata[i] != expected[i + 1]) {
			verify_error();
		}

	printf("--> Passed\r\n");
}

int main(void)
{

	/* Initializes MCU, drivers and middleware */
	atmel_start_init();
	sei();

	// Read value of BOOTEND from FUSE section of memory map
	uint8_t bootend = FUSE.BOOTEND;
	if (bootend < 8) {
		printf("BOOT section too small program, set BOOTEND fuse and retry\r\n");
		cmd_error();
	}

	// Test illegal write, BOOT should not be allowed to write to BOOT
	status = FLASH_0_write_flash_byte(0, NULL, 1);
	if (status != NVM_ERROR) {
		printf("Attempted illegal write from BOOT to BOOT\r\n");
		cmd_error();
	}

	while (1) {
		printf("Waiting for inputs.......\r\n");
		readLine();
		printf("Command received: ");
		printf(command);
		printf("\r\n");
		switch (command[0])
		{
		case '1':
			printf("MEAS_CODEC\r\n");
			MEAS_CODEC_toggle_level(); 
			break;
		case '2':
			printf("EN_CODEC\r\n");
			EN_CODEC_toggle_level(); 
			break;
		case '3':
			printf("EN_MIC\r\n");
			EN_MIC_toggle_level(); 
			break;
		case '4':
			printf("MEAS_MIC\r\n");
			MEAS_MIC_toggle_level(); 
			break;
		case '5':
			printf("MEAS_BARO\r\n");
			MEAS_BARO_toggle_level(); 
			break;
		case '6':
			printf("MEAS_IMU\r\n");
			MEAS_IMU_toggle_level(); 
			break;
		case '7':
			printf("EN_BARO\r\n");
			EN_BARO_toggle_level(); 
			break;
		case '8':
			printf("EN_IMU\r\n");
			EN_IMU_toggle_level(); 
			break;
		case '9':
			printf("MEAS_RFM\r\n");
			MEAS_RFM_toggle_level(); 
			break;
		case 'a':
			printf("MEAS_SAMD\r\n");
			MEAS_SAMD_toggle_level(); 
			break;
		case 'b':
			printf("EN_RADIO\r\n");
			EN_RADIO_toggle_level(); 
			break;
		case 'c':
			printf("EN_MEAS\r\n");
			EN_MEAS_toggle_level(); 
			break;
		case 'd':
			printf("ATT_INT\r\n");
			ATT_INT_toggle_level(); 
			break;
		case 'e':
			printf("DTR_DETECT_IN\r\n");
			DTR_DETECT_IN_toggle_level(); 
			break;
		case 'f':
			printf("DTR_DETECT_OUT\r\n");
			DTR_DETECT_OUT_toggle_level(); 
			break;
		case 'g':
			printf("A_SDA\r\n");
			A_SDA_toggle_level(); 
			break;
		case 'h':
			printf("A_SCL\r\n");
			A_SCL_toggle_level(); 
			break;
		default:
			printf("UNKNOWN command\r\n");
			
		}
		
	}
}
